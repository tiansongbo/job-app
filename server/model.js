const mongoose = require('mongoose')

const DB_URL = 'mongodb://localhost:27017/job-chat'
mongoose.connect(DB_URL)

const models = {
  user: {
    'user': {type:String, require:true},
    'pwd': {type: String, require:true},
    'type': {type: String, require: true},
    'avatar': {type: String, require: true},
    'desc': {type: String},
    // 职位名
    'title': {type: String},
    // 如果你是boss
    'company':{type:String},
    'money':{type: String}
  },
  chat: {
    'chatid':{type:String,require:true},
    'from':{type:String, require:true},
    'to':{type:String, require:true},
    'read':{type:Boolean, default:false},
    'content':{type:String,require:true,default:''},
    'create_time':{type:Number,default:new Date().getTime()}
  }
}

for (let m in models){
  mongoose.model(m, new mongoose.Schema(models[m]))
}

module.exports = {
  getModel:function(name){
    return mongoose.model(name)
  }
}

// 链接MongoDB
// mongoose.connection.on('connected', function(){
//   console.log('mongo connect success')
// })
// const User = mongoose.model('user', new mongoose.Schema({
//   user: {type:String, require: true},
//   age: {type: Number, require: true}
// }))
// User.create({
//   user: 'imooc',
//   age: 18
// }, function(err, doc){
//   if (!err) {
//     console.log(doc)
//   } else {
//     console.log(err)
//   }
// })
