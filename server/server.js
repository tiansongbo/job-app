const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const user = require('./user')
const model = require('./model')
const Chat = model.getModel('chat')
const app = express()

// work with express
const server = require('http').Server(app)
const io = require('socket.io')(server)

io.on('connection', function(socket){
  // 当前链接的socket
  socket.on('sendmsg', function(data){
    // console.log(data)
    // io.emit('recvmsg',data)
    const {from, to, msg} = data
    const chatid = [from, to].sort().join('_')
    Chat.create({chatid, from, to, content:msg},function(err,doc){
      io.emit('recvmsg', Object.assign({}, doc._doc))
    })
  })
  
})

app.use(cookieParser())
app.use(bodyParser.json())
app.use('/user',user)

server.listen(9093, function(){
  console.log('app start')
})

/*
  如何发送，端口不一致，使用proxy配置
  axios拦截器，统一Loading处理
  redux里使用异步数据，渲染页面
*/ 

/*
  Socket.io是基于事件的实时双向通信库
    1，基于websocket协议
    2，前后端通过事件进行双向通信
    3，后端可以主动推送数据
    4，现代浏览器均支持websocket协议
*/ 