import React from 'react'
import {connect} from 'react-redux'
import {Result, List,WhiteSpace, Button, Modal} from 'antd-mobile'
import browserCookie from 'browser-cookies'
import {logoutSubmit} from '../../redux/user.redux'
import {Redirect} from 'react-router-dom'
@connect(
  state=>state.user,
  {logoutSubmit}
)
class User extends React.Component{
  constructor(props){
    super(props)
    this.logout = this.logout.bind(this)
  }
  logout (){
    const alert = Modal.alert
    alert('注销', '确认退出登录吗?',[
      {text: 'Cancel', onPress:()=>console.log('cancel')},
      {text: 'Ok', onPress: ()=>{
        browserCookie.erase('userid')
        this.props.logoutSubmit()
      }}
    ])
    
  }
  render(){
    const props = this.props
    const Item = List.Item
    const Brief = Item.Brief
    return props.user?(
      <div>
        <Result
          img={<img style={{width:50}} src={require(`../../component/imgs/${props.avatar}.png`)} alt=''/>}
          title={props.user}
          message={props.type === 'boss'?props.company:null}
        >
        </Result>
        
        <List renderHeader='简介'>
          <Item
            multipleLine  
          >
            {props.title}
            {props.desc.split('\n').map(v=>(<Brief key={v}>{v}</Brief>))}
            {props.money?<Brief>{props.money}</Brief>:null}
          </Item>
        </List>
        <WhiteSpace></WhiteSpace>
        <List>
          <Button onClick={this.logout}>
            退出登录
          </Button>
        </List>
      </div>
    ):(<Redirect to={props.redirectTo}></Redirect>)
  }
}

export default User