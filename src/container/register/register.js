import React from 'react'
import Logo from '../../component/logo/logo'
import {List, Radio, InputItem, WingBlank, WhiteSpace, Button} from 'antd-mobile'
import {connect} from 'react-redux'
import {register} from '../../redux/user.redux'
import {Redirect} from 'react-router-dom'
@connect(
  state=>state.user,
  {register}
)
class Register extends React.Component{
  constructor (props){
    super(props)
    this.state={
      type: 'genius',
      user:'',
      pwd: '',
      repwd: ''
    }
    this.handleRegister = this.handleRegister.bind(this)
  }
  handleChange(key, val){
    this.setState({
      [key]:val
    })
  }
  handleRegister(){
    this.props.register(this.state)
  }
  render () {
    const RadioItem = Radio.RadioItem
    return (
      <div>
        {this.props.redirectTo ? <Redirect to={this.props.redirectTo}/> : null }
        <Logo></Logo>
        {this.props.msg ? <p className='error-msg'>{this.props.msg}</p> : null}
        <WingBlank>
          <List>
              <InputItem onChange={v=>this.handleChange('user',v)}>用户</InputItem>
              <InputItem type='password' onChange={v=>this.handleChange('pwd',v)}>密码</InputItem>
              <InputItem type='password' onChange={v=>this.handleChange('repwd',v)}>确认密码</InputItem>
            </List>
            <WhiteSpace/>
            <List>
              <RadioItem 
                checked={this.state.type === 'genius'}
                onChange={()=>this.handleChange('type','genius')}>
                求职者
              </RadioItem>
              <RadioItem 
                checked={this.state.type === 'boss'}
                onChange={()=>this.handleChange('type','boss')}>
                Boss
              </RadioItem>
            </List>
            <WhiteSpace/>
            <Button type='primary' onClick={this.handleRegister}>注册</Button>
          </WingBlank>
      </div>
    )
  }
}

export default Register