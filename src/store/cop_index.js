import {createStore} from 'redux'
// 1, 新建store 通过reducer建立

// 根据老的state和action 生成新的状态
function counter(state=0, action){
  switch(action.type){
    case 'add':
      return state+1;
    case 'remove':
      return state-1;
    default:
      return state;
  }
}

const store = createStore(counter)





function listener(){
  const current = store.getState()
  console.log(`现在有枪${current}把`)
}
store.subscribe(listener)

store.dispatch({type: 'add'})
store.dispatch({type: 'add'})
store.dispatch({type: 'remove'})

