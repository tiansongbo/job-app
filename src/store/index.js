
const ADD = 'add'
const REMOVE = 'remove'


// reducer
export function counter(state=10, action){
  switch(action.type){
    case ADD:
      return state+1;
    case REMOVE:
      return state-1;
    default:
      return state;
  }
}
// action 
export function addGUN() {
  return {type: ADD}
}

export function removeGUN(){
  return {type: REMOVE}
}

// 异步action
export function addGunAsync(){
  return dispatch => {
    setTimeout(()=>{
      dispatch(addGUN())
    }, 2000)
  }
}