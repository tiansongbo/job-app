import React from 'react'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {login, getUserData} from './Auth_redux'

@connect(
  state => state.auth,
  {login, getUserData}
)
class Auth extends React.Component{

  constructor(props){
    super(props)
    this.state = {}
  }
  componentDidMount(){
    // axios.get('/data')
    //   .then(res => {
    //     console.log(res)
    //   })
    this.props.getUserData()
  }
  render(){
    return (
      <div>
        <h2>我的名字是{this.props.user}, 年龄{this.props.age}</h2>
        {this.props.isAuth ? <Redirect to='/dashboard' /> : null}
        <h2>你没有权限，需要登录</h2>
        <button onClick={this.props.login}>登录</button>
      </div>
    )
  }
}
export default Auth
/*
  复杂redux应用，多个reducer，用combineReducers合并
*/ 