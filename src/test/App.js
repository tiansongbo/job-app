import React, { Component } from 'react';
import {Button} from 'antd-mobile'
import {connect} from 'react-redux'
import {addGUN, addGunAsync} from './store'

// const mapStatetoProps = (state)=>{
//   return {num:state}
// }
// const actionCreators = {addGUN, addGunAsync}
// App = connect(mapStatetoProps, actionCreators)(App);
@connect(
  // 你要state什么属性放到props
  state=>({num:state.counter}),
  // 你要什么方法放到props
  {addGUN, addGunAsync}
)
class App extends Component {
  render() {
    return (
      <div>
        <div>现在有{this.props.num}把</div>
        <Button onClick={this.props.addGUN} type='primary'>申请武器</Button>
        <Button onClick={this.props.addGunAsync} type='primary'>延迟申请</Button>
      </div>
    )
  }
}

export default App;
