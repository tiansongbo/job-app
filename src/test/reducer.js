// 合并所有reducer 
import {combineReducers} from 'redux'
import {counter} from './store/index.js'
import {auth} from './Auth_redux.js'

export default combineReducers({counter,auth})