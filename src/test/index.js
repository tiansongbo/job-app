import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk'
import './index.css';
// import App from './App';
import {createStore, applyMiddleware, compose} from 'redux'

import reducer from './reducer.js'
// import {counter} from './store'
// import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux'
import {BrowserRouter, Route, Redirect, Switch} from 'react-router-dom'
import Auth from './test/Auth'
import Dashboard from './Dashboard'
import './axios-config.js'


const reduxDevtools = window.devToolsExtension ? window.devToolsExtension():()=>{}

const store = createStore(reducer, compose(
  applyMiddleware(thunk),
  reduxDevtools
))


ReactDOM.render(
  (<Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route path='/login' component={Auth}></Route>
        <Route path='/dashboard' component={Dashboard}></Route>
        <Redirect to='/login'></Redirect>
      </Switch>
    </BrowserRouter>
  </Provider>),
  document.getElementById('root')
);



// store.subscribe(render)
// registerServiceWorker();
// Provider组件在应用最外层，传入store即可
// Connect 负责从外部获取组件需要的参数
// Connect 可以用装饰器的方式来写
/*
    cookie类似于一张身份卡，登录后服务器端返回，你带这cookie就
    可以访问受限资源
    页cookie的管理浏览器会自动处理
*/ 
/*
<div>
        <ul>
          <li>
            <Link to="/">一营</Link>
          </li>
          <li>
            <Link to="/two">二营</Link>
          </li>
          <li>
            <Link to="/three">三营</Link>
          </li>
          <li>
            <Link to="/test">test</Link>
          </li>
        </ul>
        <Switch>
          <Route exact={true} path='/' component={One}></Route>
          <Route path='/two' component={Two}></Route>
          <Route path='/three' component={Three}></Route>
          <Route path='/:location' component={Test}></Route>
        </Switch>
        <App />
      </div>
*/