/*
    React组件优化
        属性传递优化
            每次渲染render，bind都会执行一次
        多组件优化
            定制shouldComponentUpdate
        key
            尽量不要用index作为key
    immutable-js 怎么在js里引入不可变数据结构
        对象用Map包裹Map({name: 1})
        is直接比较复杂对象是否相等
    定制shouldComponetUpdate
    Ant Motion
*/ 
import React from 'react'
import {Map, is} from 'immutable'

let obj = Map({
    'name': 'imooc',
    'course': Map({name: 'react+redux'})
})

let obj1 = obj.set('name', 'woniu')
// console.log(obj1 === obj)
// console.log(obj1.get('course') === obj.get('course'))

let ob1 = Map({name: 1, title: 'imooc'})
let ob2 = Map({name: 1, title: 'imooc'})
console.log(is(ob1, ob2))


class DiyReactRedux extends React.Component{
    constructor(props){
        super(props)
        this.state = Map({
            num : 1,
            title: 'imooc'
        })
        this.handleClick = this.handleClick.bind(this)
    }
    handleClick(){
        this.setState(this.state.set('num', 3))
    }
    
    render () {
        const user = '蜗牛'
        return (
            <div>
                <h1>我是{this.state.get('num')}</h1>
                <button onClick={this.handleClick}>加1</button>
                <Demo title={this.state.get('title')}></Demo>
            </div>
        )
    }
}
class Demo extends React.Component{
    shouldComponentUpdate(nextProps, nextState){
        return is(nextProps, nextState)
    }
    render(){
        console.log('11111111')
        return (
            
            <div>
                num: {this.props.title}
            </div>
        )
    }
}
export default DiyReactRedux