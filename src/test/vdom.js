/*
    inital render
        constructor()
        componentWillMount()
        render()
        componentDidMount()
        componentWillUnmount()

    父组件render
        componentWillReceiveProps()

        调用setState执行的生命周期函数
            shouldComponentUpdate()定制
                返回true才往下执行
            componentWillUpdate()
            render()
            componentDidUpdate()
        componentWillUnmount
    
    this.state.num = 1 
    这样的写法是做不了任何更新的
    setState是异步更新的
    
    不要在render函数中执行setState(),执行setState()会执行render函数，
    除非有定制的shouldComponentUpdate函数，终止render
    setState是异步的，会存到队列中，
*/ 

/*
    const store = createStore(counter)

    const init = store.getState()

    console.log('一开始有机枪x把')

    store.subscribe(listener)

    store.dispatch({type: 'add'})
*/ 