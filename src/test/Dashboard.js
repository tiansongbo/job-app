import React from 'react'
import {Route, Link, Switch, Redirect} from 'react-router-dom'
import {connect} from 'react-redux'
import {logout} from './Auth_redux'
import App from './App';
function One (){
  return <h2>ONE</h2>
}
function Two (){
  return <h2>Two</h2>
}
function Three (){
  return <h2>Three</h2>
}

@connect(
  state=>state.auth,
  {logout}
)

class Dashboard extends React.Component{
  constructor(props){
    super(props)
    this.state = {}
  }
  render(){
    const match = this.props.match
    const redirectToLogin = <Redirect to='/login'></Redirect>
    const app = (
      <div>
        {this.props.isAuth ? <button onClick={this.props.logout}>注销</button> : null}
        <ul>
          <li>
            <Link to={`${match.url}`}>一营</Link>
          </li>
          <li>
            <Link to={`${match.url}/two`}>二营</Link>
          </li>
          <li>
            <Link to={`${match.url}/three`}>三营</Link>
          </li>
        </ul>
          <Switch>
            <Route exact path={`${match.url}`} component={One}></Route>
            <Route path={`${match.url}/two`} component={Two}></Route>
            <Route path={`${match.url}/three`} component={Three}></Route>
          </Switch>
        <App />
      </div>
    )

    return this.props.isAuth ? app : redirectToLogin;
  }
}
export default Dashboard