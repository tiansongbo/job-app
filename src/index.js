import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk'
import {createStore, applyMiddleware, compose} from 'redux'
import reducer from './reducer.js'
import {Provider} from 'react-redux'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import './axios-config.js'
import Login from './container/login/login'
import Register from './container/register/register'
import AuthRoute from './component/authrouter/authrouter'
import './index.css'
import BossInfo from './container/bossinfo/bossinfo'
import GeniuInfo from './container/geniusinfo/geniusinfo'
import Dashboard from './component/dashboard/dashboard'
import Chat from './component/chat/chat'
import DiyReactRedux from './test/xingneng.js'
// import Home from './routes/Home'
const reduxDevtools = window.devToolsExtension ? window.devToolsExtension():f=>f
const store = createStore(reducer, compose(
  applyMiddleware(thunk),
  reduxDevtools
))

ReactDOM.render(
  (<Provider store={store}>
    <BrowserRouter>
      <div>
        <AuthRoute></AuthRoute>
        <Switch>
          <Route exact path='/' component={Login}></Route>
          <Route path='/bossinfo' component={BossInfo}></Route>
          <Route path='/geniusinfo' component={GeniuInfo}></Route>
          <Route path='/login' component={Login}></Route>
          <Route path='/register' component={Register}></Route>
          <Route path='/chat/:user' component={Chat}></Route>
          <Route path='/test' component={DiyReactRedux}></Route>
          <Route component={Dashboard}></Route>
        </Switch>
      </div>
    </BrowserRouter>
  </Provider>),
  document.getElementById('root')
);
